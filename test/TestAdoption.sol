// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.7.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Adoption.sol";

contract TestAdoption {
    //address of the adoption contract to be tested
    Adoption adoption = Adoption(DeployedAddresses.Adoption());

    //id of pet that will be used for testing
    uint expectedPetId = 8;

    //the expected owner of adopted pet is this contract
    address expectedAdopter = address(this);


    //Testing the adopt() function
    function testUserCanAdoptPet() public{
        uint returnedId = adoption.adopt(expectedPetId, 0.1 ether);

        Assert.equal(returnedId, expectedPetId, "Adoption of the expected pet should match what is returned.");
    }

    /*function testUserCantAdoptPet() public{
        string memory err;
        string memory expectedErr = "Error: VM Exception while processing transaction: revert";

        try  Adoption(DeployedAddresses.Adoption()).adopt(expectedPetId,0) returns (uint v){
            uint returnedInt = v;
        }
        catch Error(string memory e) {
            //err = e;
            Assert.equal(e, expectedErr, "error message.");
        }
        Assert.equal(err, expectedErr, "error message.");

        //   Assert.isFalse(returnedId, expectedPetId, "Adoption of the expected pet should match what is returned.");

    }*/

    function testGetAdopterAddressByPetId() public{
        address adopter = adoption.adopters(expectedPetId);

        Assert.equal(adopter, expectedAdopter, "Owner of the expected pet should be this contract.");
    }

    function testGetAdopterAddressByPetIdInArray() public{
       //store adopters in memory rather than contract's storage
        address[16] memory adopters = adoption.getAdopters();

        Assert.equal(adopters[expectedPetId], expectedAdopter, "Owner of the expected pet should be this contract.");
    }
}
