// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.7.0;

contract Adoption {
    address[16] public adopters;

    //adopting a pet
    function adopt(uint petId) public payable returns (uint) {
        require(petId >= 0 && petId <= 15 && msg.value > 0 ether);
        adopters[petId] = msg.sender;
        return petId;
    }

    //retrieving the adopters
    function getAdopters() public view returns(address[16] memory){
        return adopters;
    }

}
