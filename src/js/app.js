App = {
  web3Provider: null,
  contracts: {},

  init: async function() {
    // Load pets.
    $.getJSON('../pets.json', function(data) {
      var petsRow = $('#petsRow');
      var petTemplate = $('#petTemplate');

      for (i = 0; i < data.length; i ++) {
        petTemplate.find('.panel-title').text(data[i].name);
        petTemplate.find('img').attr('src', data[i].picture);
        petTemplate.find('.pet-breed').text(data[i].breed);
        petTemplate.find('.pet-age').text(data[i].age);
        petTemplate.find('.pet-location').text(data[i].location);
        petTemplate.find('.btn-adopt').attr('data-id', data[i].id);
        petTemplate.find('.adopt-amount').attr('data-pet-id', data[i].id);
        petsRow.append(petTemplate.html());
      }
    });

    return await App.initWeb3();
  },

  initWeb3: async function() {
    if(window.ethereum){
        App.web3Provider = window.ethereum;
        try{
            await window.ethereum.enable();
        } catch(error){
        console.error("User denied account access")
        }
    }
    else if(window.web3){
        App.web3Provider = window.web3.currentProvider;
    }
    else{ //fallback but insecure and not suitable for production
        App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }
    web3 = new Web3(App.web3Provider);
    return App.initContract();
  },

  initContract: function() {
    $.getJSON('Adoption.json', function(data){
        var AdoptionArtifact = data;
        App.contracts.Adoption = TruffleContract(AdoptionArtifact);

        App.contracts.Adoption.setProvider(App.web3Provider);

        return App.markAdopted();
    });

    return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '.btn-adopt', App.handleAdopt);
    $(document).on('keyup', '.adopt-amount', App.enableAdopt);

  },

  enableAdopt: function(){
      var petId = parseInt($(this).data('pet-id'));
      var button = $('button[data-id="'+petId+'"]');
     if ($(this).val() <= 0) {
         button.prop('disabled', true);
     } else {
         button.prop('disabled', false);
     }
  },

  markAdopted: function(adopters, account) {
    var adoptionInstance;

    App.contracts.Adoption.deployed().then(function(instance) {
      adoptionInstance = instance;

      return adoptionInstance.getAdopters.call();
    }).then(function(adopters) {
      for (i = 0; i < adopters.length; i++) {
        if (adopters[i] !== '0x0000000000000000000000000000000000000000') {
          successfulBtn = $('.panel-pet').eq(i).find('button').text('Adopted')
          successfulBtn.attr('disabled', true);
          $('input[data-pet-id="'+successfulBtn.data('id')+'"]').hide();
        }
      }
    }).catch(function(err) {
      console.log(err.message);
    });
  },

  handleAdopt: function(event) {
    event.preventDefault();

    var petId = parseInt($(event.target).data('id'));
    var amount = $('input[data-pet-id="'+petId+'"]').val();
    console.log(amount);
    var adoptionInstance;

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];

      App.contracts.Adoption.deployed().then(function(instance) {
        adoptionInstance = instance;

        // Execute adopt as a transaction by sending account
        return adoptionInstance.adopt(petId, {from: account, value:web3.toWei(amount, "ether")});
      }).then(function(result) {
        return App.markAdopted();
      }).catch(function(err) {
        console.log(err.message);
      });
    });
  }



};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
